// Bài 1
// Input: Khai báo biến và Lấy giá trị người dùng nhập vào
// Process: test case
// - num1 < num2 < num3
// - num1 < num3 < num2
// - num2 < num1 < num3
// - num2 < num3 < num1
// - num3 < num1 < num2 
// Output  :  sắp xếp theo thứ tự tăng dần

function xepSo(){
    var num1 = document.getElementById("txt-thu-nhat").value * 1;
    var num2 = document.getElementById("txt-thu-hai").value * 1;
    var num3 = document.getElementById("txt-thu-ba").value * 1;

    if(num1 < num2 && num1 < num3 && num2 < num3  ) {
        document.getElementById("demo").innerHTML= `${num1} < ${num2} <${num3}`
    } else if(num1 < num2 && num1 < num3 && num3 < num2){
        document.getElementById("demo").innerHTML= `${num1} < ${num3} <${num2}`
    } else if (num2 < num3 && num2 < num1 && num1 < num3){
        document.getElementById("demo").innerHTML= `${num2} < ${num1} <${num3}`
    } else if (num2 < num3 && num2 < num1 && num3 < num1){
        document.getElementById("demo").innerHTML= `${num2} < ${num3} <${num1}`
    } else if(num3 < num1 && num3 < num2 && num2 < num1) {
        document.getElementById("demo").innerHTML= `${num3} < ${num2} <${num1}`
    } else {
        document.getElementById("demo").innerHTML= `${num3} < ${num1} <${num2}`
    }}


// Bài 2
// Input: Khai báo biến và lấy dữ liệu mục đc chọn
// Process: so sánh giá trị với mục được được chọn
// Output  :  Hiển thị lời chào thích hợp

function guiLoiChao(){
    var Bo = "B";
    var Me = "M";
    var anhTrai = "A";
    var emGai = "E";

    var guiLoiValue = document.getElementById("dragDown").value; 
    if(guiLoiValue == Bo){
        document.getElementById("demo2").innerHTML = ` Xin chào Bố!`
    }
    else if(guiLoiValue == Me){
        document.getElementById("demo2").innerHTML = ` Xin chào Mẹ!`
    }
    else if(guiLoiValue == anhTrai){
        document.getElementById("demo2").innerHTML = ` Xin chào Anh Trai!`
    }
    else if(guiLoiValue == emGai){
        document.getElementById("demo2").innerHTML = ` Xin chào Em Gái!`
    }
    else{
        document.getElementById("demo2").innerHTML = ` Xin chào Người lạ ơi!`
    }
} 
// Bai 3
function demSo(){
      var s1 = document.getElementById("txt-thu-f").value*1;
      var s2 = document.getElementById("txt-thu-s").value*1;
      var s3 = document.getElementById("txt-thu-t").value*1;
      var soChan = 0;
      var soLe = 0;
}

// Bài 4
// Input: Khai báo biến và lấy dữ liệu mục đc chọn
// Process: Test case các trường hợp ( bên dưới )
// Output  :  Hiển thị tam giác phù hợp với số liệu
function duDoan(){
    var ed1 = document.getElementById("txt-edge-mot").value*1;
    var ed2 = document.getElementById("txt-edge-hai").value*1;
    var ed3 = document.getElementById("txt-edge-ba").value*1;

    if(ed1 === ed2 && ed2 === ed3){
       document.getElementById("demo4").innerHTML = `Hình tam giác đều`
    } else if (ed1 == ed2 && ed3 !== ed1 && ed3 !== ed2 ){
        document.getElementById("demo4").innerHTML = `Hình tam giác cân`
    } else if (ed2 == ed3 && ed1 !== ed2 && ed1 !== ed3){
        document.getElementById("demo4").innerHTML = `Hình tam giác cân`
    } else if (ed3 == ed1 && ed2 !== ed1 && ed2 !== ed3){
        document.getElementById("demo4").innerHTML = `Hình tam giác cân`
    } else if (ed1^2 == ed2^2 + ed3^2){
        document.getElementById("demo4").innerHTML = `Hình tam giác vuông`
    } else if(ed2^2 == ed1^2 + ed3^2 ){
        document.getElementById("demo4").innerHTML = `Hình tam giác vuông`
    } else if(ed3^2 == ed1^2 + ed2^2){
        document.getElementById("demo4").innerHTML = `Hình tam giác vuông`
    }else{
        document.getElementById("demo4").innerHTML = `Một loại tam giác nào đó`  
    }
}



